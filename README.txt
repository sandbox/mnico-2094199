RESUME

A very small module that extends the functionality of the object 
Drupal.behaviors. Allows other developers to define and invoke custom behaviors 
from very specific events that make their modules.
