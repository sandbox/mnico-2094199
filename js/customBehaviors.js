/**
 * @file
 */

(function($) {

/**
 * This function is like Drupal.attachBehaviors() but the developer can define
 * the name of the function to execute and when do it.
 *
 * Example:
 *
 * Drupal.behaviors.SomeModule = {
 *  attach: function(context, settings) {
 *    $('body', context).click(function() {
 *      // Here define the new function to execute on each object
 *      // Drupal.behaivors.
 *      Drupal.invokeBehaviors('bodyClicked', [variable1, variable2, context, settings]);
 *    });
 *  }
 * }
 *
 * Another module:
 *
 * Drupal.behaviors.AnotherModule = {
 *   attach: function(context, settings) {
 *     ... Do something or not.
 *   },
 *   bodyClicked: function(variable1, variable2, context, settings) {
 *     ... Do something when the user click on the body element.
 *   }
 * }
 *};
 *
 * @param functionName
 *   The name of the function to be executed on each object Drupal.behaivors.
 * @param args
 *   An array of arguments.
 *
 */
Drupal.invokeBehaviors = function (functionName, args) {
  // Execute all behaviors that use the functionName.
  $.each(Drupal.behaviors, function () {
    if ($.isFunction(this[functionName])) {
      this[functionName].apply(this, args);
    }
  });
};

})(jQuery);

