

(function($) {

  /**
   * This new behavior is triggered once the user has clicked in a row.
   */
  Drupal.behaviors.CustomBehaviorsExample = {
    attach: function(context, settings) {
      // The user clicked in a row.
      $('.row-table-custom-behavarios-example', context).click(function() {
        id = $(this).attr('rel');
        // Invoke the behavior.
        Drupal.invokeBehaviors('customBehaviorsExampleClickRow', [id, context, settings]);
      });
    }
  };

  /**
   * These objects can be in other modules.
   */
  Drupal.behaviors.BehaviorOne = {
    // Use the default event onload
    attach: function(context, settings) {
      $('#custom-behaviors-example-result').append('<div>BehaviorOne - Just print the value : <span id="behavior-one-click"></span></div>');
    },
    // Use the custom behavior.
    customBehaviorsExampleClickRow: function(id, context, settings) {
      $('#behavior-one-click').text(id);
    }
  };

  Drupal.behaviors.BehaviorTwo = {
    // Use the default event onload
    attach: function(context, settings) {
      $('#custom-behaviors-example-result').append('<div>BehaviorTwo - We will sum the value : <span id="behavior-two-click"></span></div>');
    },
    // Use the custom behavior.
    customBehaviorsExampleClickRow: function(id, context, settings) {
      var sum = parseInt(id) + parseInt(id);
      var text = id + '+' + id + '=' + sum;
      $('#behavior-two-click').text(text);
    }
  };
})(jQuery);